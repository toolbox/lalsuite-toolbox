FROM containers.ligo.org/toolbox/igwn-toolbox:el8

# setup rpm macros
COPY /environment/.rpmmacros /root/.rpmmacros

# install development tools
RUN dnf -y group install "Development Tools" && \
    dnf clean all

# install lalsuite dependencies
RUN dnf -y install igwn-lalsuite-devel && \
    dnf clean all

# git-lfs post-install
RUN git lfs install
